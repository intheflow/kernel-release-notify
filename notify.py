#!/usr/bin/env python3

import argparse
import subprocess
from pathlib import Path

import requests

from config import MAIL_BODY, MAIL_HEADER, RECIPIENTS

parser = argparse.ArgumentParser(description='Check current kernel release \
        version, send email if changed.')
parser.add_argument(
        '-f', '--force',
        action='store_true', help='ignore previous version')

arguments = parser.parse_args()


def get_release_info(releases_json, version):
    for release in releases_json['releases']:
        if release['version'] == version:
            return release


last_version_path = Path('kernel-version')

if last_version_path.exists():
    with open('kernel-version', 'r') as f:
        old_version = f.read()
else:
    old_version = 'unknown'

r = requests.get('https://www.kernel.org/releases.json')
releases_json = r.json()
new_version = releases_json['latest_stable']['version']
release_info = get_release_info(releases_json, new_version)

if new_version != old_version or arguments.force:
    with open('kernel-version', 'w') as f:
        f.write(new_version)

    header = MAIL_HEADER.format(
        new_version=new_version, old_version=old_version, **release_info)
    body = MAIL_BODY.format(
        new_version=new_version, old_version=old_version, **release_info)
    text = '{}\n\n{}'.format(header, body)
    subprocess.run(
        ['/usr/sbin/sendmail', *RECIPIENTS],
        input=text.encode('utf-8'),
        check=True,
        stdout=subprocess.PIPE)
